/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organiclanka_pvt.common;

/**
 *
 * @author kasun
 */
public class SystemVariableList {

    public static final String REPORT_BASE_URL = System.getProperties().getProperty("user.dir").replace("\\", "/") + "/OrganicLankaPvt_reports/";
//    public static final String REPORT_BASE_URL = "D:\\OrganicLanka\\OrganicLankaPvt_reports\\";
    public static final String LOGGER_BASE_URL = System.getProperties().getProperty("user.dir").replace("\\", "/") + "/loggers/OrganicLanaka_logger.log";
//    public static final String LOGGER_BASE_URL = "D:\\OrganicLanka\\loggers\\OrganicLanaka_logger.log";
}
