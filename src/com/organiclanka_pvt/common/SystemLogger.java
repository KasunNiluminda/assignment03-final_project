/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organiclanka_pvt.common;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

/**
 *
 * @author kasun
 */
public class SystemLogger {

    private static Logger log;

    public static void initLogger() {
        try {
            String path = SystemVariableList.LOGGER_BASE_URL;
            PatternLayout patternLayout = new PatternLayout("%-5p %d{yyyy-MMM-dd HH:mm:ss} %C %m %n");
            //  HTMLLayout patternLayout = new HTMLLayout();
            RollingFileAppender appender = new RollingFileAppender(patternLayout, path);
            appender.setMaxFileSize("10MB");
            appender.setName("OrganicLankaLogger");
            appender.activateOptions();
            Logger.getRootLogger().addAppender(appender);
        } catch (IOException e) {
            log.error("SystemLogger file path error : ", e);
        }

    }
}
