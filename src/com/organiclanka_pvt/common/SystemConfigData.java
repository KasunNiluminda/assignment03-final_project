/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organiclanka_pvt.common;

/**
 *
 * @author DELL
 */
public class SystemConfigData {

    private static String activeUser;
    private static String userType;
    private static String empId;
    private static int sectionId;
    private static String resetUser;
    private static int smsCode;

    public static String getActiveUser() {
        return activeUser;
    }

    public static void setActiveUser(String aActiveUser) {
        activeUser = aActiveUser;
    }

    public static String getUserType() {
        return userType;
    }

    public static void setUserType(String aUserType) {
        userType = aUserType;
    }

    public static String getEmpId() {
        return empId;
    }

    public static void setEmpId(String aEmpId) {
        empId = aEmpId;
    }

    public static String getResetUser() {
        return resetUser;
    }

    public static void setResetUser(String aResetUser) {
        resetUser = aResetUser;
    }

    public static int getSmsCode() {
        return smsCode;
    }

    public static void setSmsCode(int aSmsCode) {
        smsCode = aSmsCode;
    }

    public static int getSectionId() {
        return sectionId;
    }

    public static void setSectionId(int aSectionId) {
        sectionId = aSectionId;
    }
}
