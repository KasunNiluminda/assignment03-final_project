/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organiclanka_pvt.common;

/**
 *
 * @author kasun
 */
public class FirstLetterConvertUpperCase {

    public static String getCovertedText(String text) {
        String formatedText = "";
        if (!text.trim().equals("")) {
            String arrayText[] = text.trim().split(" ");
            if (arrayText.length >= 1) {
                for (int i = 0; arrayText.length > i; i++) {
                    if (arrayText[i].length() >= 1) {
                        String firstLetter = arrayText[i].substring(0, 1).toUpperCase();
                        String otherLetter = arrayText[i].substring(1, arrayText[i].length()).toLowerCase();
                        if (i == 0) {
                            formatedText = firstLetter + otherLetter;
                        } else {
                            String OtherText = firstLetter + otherLetter;
                            formatedText += " " + OtherText;
                        }
                    }
                }
            }
        }
        return formatedText;
    }
}
