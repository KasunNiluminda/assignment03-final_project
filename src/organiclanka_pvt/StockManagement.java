/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package organiclanka_pvt;

import com.organiclanka_pvt.db.DB;
import java.awt.Color;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 *
 * @author DELL
 */
public class StockManagement extends javax.swing.JFrame {

    private Logger log;
    static StockAddingView stockAdd;
    static StockView stockView;

    /**
     * Creates new form StockManagement
     */
    public StockManagement() {
        initComponents();
        messageBoxGRNList();
        messageBoxStockCount();
        messageBoxExprireDateDiffrence();
        log = Logger.getLogger("OrganicLankaLogger");
        log.info("");
        log.info("Stock Mangement is starting...");
        //   panel_ExpireDateMsgBox.setVisible(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btn_addStock = new javax.swing.JButton();
        btn_updateStock = new javax.swing.JButton();
        btn_itemRegistration = new javax.swing.JButton();
        btn_viewStock = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        panel_GRNMsgBox = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        btn_GRNMsgBox = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        panel_ExpireDateMsgBox = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        btn_expireMsgBox = new javax.swing.JButton();
        panel_stockCountMsgBox = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        btn_stockCountMsgBox = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        btn_close = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Stock Management - Oraganica Lanka 1.0");
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com.organiclanka_pvt.image/Icon03.png"))
        );
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Verdana", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Stock Management");

        btn_addStock.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        btn_addStock.setText("Add Stock");
        btn_addStock.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_addStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addStockActionPerformed(evt);
            }
        });

        btn_updateStock.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        btn_updateStock.setText("Update Stock");
        btn_updateStock.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_updateStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateStockActionPerformed(evt);
            }
        });

        btn_itemRegistration.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        btn_itemRegistration.setText("Item Registration");
        btn_itemRegistration.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_itemRegistration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_itemRegistrationActionPerformed(evt);
            }
        });

        btn_viewStock.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        btn_viewStock.setText("View Stock");
        btn_viewStock.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_viewStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewStockActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message Box", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 3, 18))); // NOI18N

        panel_GRNMsgBox.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel2.setFont(new java.awt.Font("Tahoma", 2, 20)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btn_GRNMsgBox.setFont(new java.awt.Font("Tahoma", 3, 16)); // NOI18N
        btn_GRNMsgBox.setText("--Click to View--");
        btn_GRNMsgBox.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_GRNMsgBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_GRNMsgBoxActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 2, 20)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("This GRN list is not stocked");

        javax.swing.GroupLayout panel_GRNMsgBoxLayout = new javax.swing.GroupLayout(panel_GRNMsgBox);
        panel_GRNMsgBox.setLayout(panel_GRNMsgBoxLayout);
        panel_GRNMsgBoxLayout.setHorizontalGroup(
            panel_GRNMsgBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
            .addGroup(panel_GRNMsgBoxLayout.createSequentialGroup()
                .addGap(90, 90, 90)
                .addComponent(btn_GRNMsgBox, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panel_GRNMsgBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panel_GRNMsgBoxLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        panel_GRNMsgBoxLayout.setVerticalGroup(
            panel_GRNMsgBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_GRNMsgBoxLayout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addComponent(btn_GRNMsgBox, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panel_GRNMsgBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panel_GRNMsgBoxLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(183, Short.MAX_VALUE)))
        );

        panel_ExpireDateMsgBox.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel3.setFont(new java.awt.Font("Tahoma", 2, 20)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("<html><p>The expiration date of this item </p><p> in stock expires in two months.</p></html>");

        btn_expireMsgBox.setFont(new java.awt.Font("Tahoma", 3, 16)); // NOI18N
        btn_expireMsgBox.setText("--Click to View--");
        btn_expireMsgBox.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_expireMsgBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_expireMsgBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_ExpireDateMsgBoxLayout = new javax.swing.GroupLayout(panel_ExpireDateMsgBox);
        panel_ExpireDateMsgBox.setLayout(panel_ExpireDateMsgBoxLayout);
        panel_ExpireDateMsgBoxLayout.setHorizontalGroup(
            panel_ExpireDateMsgBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE)
            .addGroup(panel_ExpireDateMsgBoxLayout.createSequentialGroup()
                .addGap(96, 96, 96)
                .addComponent(btn_expireMsgBox, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panel_ExpireDateMsgBoxLayout.setVerticalGroup(
            panel_ExpireDateMsgBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_ExpireDateMsgBoxLayout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(btn_expireMsgBox, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        panel_stockCountMsgBox.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel4.setFont(new java.awt.Font("Tahoma", 2, 20)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("<html><p>The item count in the stock is</p><p>is less than 20</p></html>");

        btn_stockCountMsgBox.setFont(new java.awt.Font("Tahoma", 3, 16)); // NOI18N
        btn_stockCountMsgBox.setText("--Click to View--");
        btn_stockCountMsgBox.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_stockCountMsgBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stockCountMsgBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_stockCountMsgBoxLayout = new javax.swing.GroupLayout(panel_stockCountMsgBox);
        panel_stockCountMsgBox.setLayout(panel_stockCountMsgBoxLayout);
        panel_stockCountMsgBoxLayout.setHorizontalGroup(
            panel_stockCountMsgBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 377, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_stockCountMsgBoxLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_stockCountMsgBox, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(89, 89, 89))
        );
        panel_stockCountMsgBoxLayout.setVerticalGroup(
            panel_stockCountMsgBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_stockCountMsgBoxLayout.createSequentialGroup()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(btn_stockCountMsgBox, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel_GRNMsgBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel_ExpireDateMsgBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel_stockCountMsgBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(37, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panel_stockCountMsgBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel_ExpireDateMsgBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel_GRNMsgBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(37, 37, 37))
        );

        btn_cancel.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        btn_close.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        btn_close.setText("Close");
        btn_close.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btn_closeMouseMoved(evt);
            }
        });
        btn_close.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_closeMouseExited(evt);
            }
        });
        btn_close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_closeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(91, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btn_addStock, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(47, 47, 47)
                        .addComponent(btn_updateStock, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46)
                        .addComponent(btn_itemRegistration, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48)
                        .addComponent(btn_viewStock, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btn_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(btn_close, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(93, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                .addGap(108, 108, 108)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_addStock, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_updateStock, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_itemRegistration, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_viewStock, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 89, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_close, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_updateStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateStockActionPerformed
        UpdateStockView updateStock = new UpdateStockView();
        updateStock.setVisible(true);
        dispose();
    }//GEN-LAST:event_btn_updateStockActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        HomeView.stockMgt = null;
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_closeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_closeActionPerformed
        HomeView.stockMgt = null;
        dispose();
    }//GEN-LAST:event_btn_closeActionPerformed

    private void btn_addStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addStockActionPerformed
//        StockAddingView stockAdd = new StockAddingView("StockMgt");
//        stockAdd.setVisible(true);

        if (stockAdd == null) {
            stockAdd = new StockAddingView("StockMgt");
        }
        stockAdd.setVisible(true);
        if (stockAdd.getState() == 1) {//minimise state = 1 and NORMAL = 0
            stockAdd.setState(NORMAL);
        }
        dispose();
    }//GEN-LAST:event_btn_addStockActionPerformed

    private void btn_itemRegistrationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_itemRegistrationActionPerformed
        ItemRegistration itemRegistration = new ItemRegistration("stockMGT");
        itemRegistration.setVisible(true);
        dispose();
    }//GEN-LAST:event_btn_itemRegistrationActionPerformed

    private void btn_viewStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewStockActionPerformed
//        StockView stockView = new StockView();
//        stockView.setVisible(true);
        if (stockView == null) {
            stockView = new StockView();
        }
        stockView.setVisible(true);
        if (stockView.getState() == 1) {//minimise state = 1 and NORMAL = 0
            stockView.setState(NORMAL);
        }
        dispose();
    }//GEN-LAST:event_btn_viewStockActionPerformed

    private void btn_GRNMsgBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_GRNMsgBoxActionPerformed
        StockAddingView stockAdd = new StockAddingView("MsgBoxGRN");
        stockAdd.setVisible(true);
        dispose();
    }//GEN-LAST:event_btn_GRNMsgBoxActionPerformed

    private void btn_expireMsgBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_expireMsgBoxActionPerformed
        StockView stockView = new StockView("MsgBoxExpireDate");
        stockView.setVisible(true);
        dispose();
    }//GEN-LAST:event_btn_expireMsgBoxActionPerformed

    private void btn_stockCountMsgBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stockCountMsgBoxActionPerformed
        StockView stockView = new StockView();
        stockView.setVisible(true);
        dispose();
    }//GEN-LAST:event_btn_stockCountMsgBoxActionPerformed

    private void btn_closeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_closeMouseExited
        btn_close.setForeground(Color.black);
        btn_close.setBackground(Color.LIGHT_GRAY);
    }//GEN-LAST:event_btn_closeMouseExited

    private void btn_closeMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_closeMouseMoved
        btn_close.setForeground(Color.white);
        btn_close.setBackground(Color.red);
    }//GEN-LAST:event_btn_closeMouseMoved

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        HomeView.stockMgt = null;
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(StockManagement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(StockManagement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(StockManagement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(StockManagement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new StockManagement().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_GRNMsgBox;
    private javax.swing.JButton btn_addStock;
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_close;
    private javax.swing.JButton btn_expireMsgBox;
    private javax.swing.JButton btn_itemRegistration;
    private javax.swing.JButton btn_stockCountMsgBox;
    private javax.swing.JButton btn_updateStock;
    private javax.swing.JButton btn_viewStock;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel panel_ExpireDateMsgBox;
    private javax.swing.JPanel panel_GRNMsgBox;
    private javax.swing.JPanel panel_stockCountMsgBox;
    // End of variables declaration//GEN-END:variables

    private void messageBoxGRNList() {
        try {
            ResultSet rsShop = DB.search("select stock_added from shop_grn_item where stock_added = '0'");
            ResultSet rsPlant = DB.search("select stock_added from plant_grn_item where stock_added = '0'");
            if (rsPlant.next() || rsShop.next()) {
                panel_GRNMsgBox.setVisible(true);
                panel_GRNMsgBox.setBackground(Color.yellow);
            } else {
                panel_GRNMsgBox.setVisible(false);
            }
        } catch (Exception ex) {
            log.error("Exception thrown in messageBoxGRNList() method", ex);
        }
    }

    private void messageBoxStockCount() {
        try {
            ResultSet rsShop = DB.search("select qty from shop_stock where qty <= '20' AND stat = '1' ");
            ResultSet rsPlant = DB.search("select qty from plant_stock where qty <= '20' AND stat = '1'");
            if (rsPlant.next() || rsShop.next()) {
                panel_stockCountMsgBox.setVisible(true);
                panel_stockCountMsgBox.setBackground(Color.orange);
            } else {
                panel_stockCountMsgBox.setVisible(false);
            }
        } catch (Exception ex) {
            log.error("Exception thrown in messageBoxStockCount() method", ex);
        }
    }

    private void messageBoxExprireDateDiffrence() {
        try {
            ResultSet rsShop = DB.search("select distinct expire_date from shop_stock where stat = '1'");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date currentDate01 = new Date();
            sdf.format(currentDate01);
            Date currentDate02 = new SimpleDateFormat("yyyy-MM-dd").parse(sdf.format(currentDate01));
            long currentTime = sdf.parse(sdf.format(currentDate02)).getTime();
            int x = 0;
            while (rsShop.next()) {
                String expireDate01 = rsShop.getString("expire_date");
                if (!expireDate01.equals("null")) {
                    Date expireDate02 = new SimpleDateFormat("yyyy-MM-dd").parse(expireDate01);
                    long expTime = sdf.parse(sdf.format(expireDate02)).getTime();
                    long diff = expTime - currentTime;
                    long dateDiff = diff / (1000 * 60 * 60 * 24);

                    if (dateDiff <= 60) {
                        x = 2;
                        panel_ExpireDateMsgBox.setVisible(true);
                        panel_ExpireDateMsgBox.setBackground(Color.pink);
                        break;
                    }
                }
            }
            if (x == 0) {
                panel_ExpireDateMsgBox.setVisible(false);
            }
        } catch (Exception ex) {
            log.error("Exception thrown in messageBoxExprireDateDiffrence() method", ex);
        }
    }
}
