/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organiclanka_pvt.common;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kasun
 */
public class FirstLetterConvertUpperCaseIT {

    public FirstLetterConvertUpperCaseIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCovertedText method, of class FirstLetterConvertUpperCase.
     */
    @Test
    public void testGetCovertedText() {
        System.out.println("getCovertedText");
        String text = "kasun niluminda";
        String expResult = "Kasun Niluminda";
        String result = FirstLetterConvertUpperCase.getCovertedText(text);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

}
